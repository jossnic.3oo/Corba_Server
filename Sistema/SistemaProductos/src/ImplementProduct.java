import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import org.omg.CORBA.ORB;
import Connection.ConnectionSQLITE;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author José Santos
 */
public class ImplementProduct extends ProductApp.ProductPOA {
    private ORB orb;
    public ConnectionSQLITE c = new ConnectionSQLITE();
    public Connection cn = c.get();

    public void setOrb(ORB orb) {
        this.orb = orb;
    }
    
    @Override
    public String listProducts(String category) {
        String list = "";
        try {
            
            /*
            Class.forName("com.mysql.jdbc.Driver");
            
            Connection cn = DriverManager.getConnection("jdbc:mysql://localhost:80/bd_corba", "root", "");
*/            
            String sentence = "select p.code as CODIGO, p.description as DESCRIPCION,"
                    + "c.descriptionCategory as CATEGORIA, p.priceLastPurchase as PRECIO,"
                    + "p.stock as STOCK from Product p join Category c "
                    + "on p.codeCategory = c.codeCategory where c.descriptionCategory "
                    + "like '" + category + "';";
            
            Statement st = cn.createStatement();
            
            ResultSet rs = st.executeQuery("select * from product;");
            while(rs.next()) {
                list += rs.getInt(1) + "%" + rs.getString(2)+ "%" + rs.getString(3)+ "%" + rs.getString(4)+ "%" + rs.getString(5);
            }
            
            rs.close();
        }
        catch(Exception exc) {
            JOptionPane.showMessageDialog(null, exc.getMessage());
        }
        
        return list;
    }
    
    @Override
    public boolean eliminateProduct(String code) {
        boolean result = false;
        try {/*
            Class.forName("com.mysql.jdbc.Driver");
            
            Connection cn = DriverManager.getConnection("jdbc:mysql://localhost:80/bd_corba", "root", "");
            */
            String sentence = "delete from Product where code = '" + code + "'";
            
            Statement st = cn.createStatement();
            
            int value = st.executeUpdate(sentence);
            
            if(value <= 0) {
                result = false;
            }else {
                result = true;
            }
            
            st.close();
        }
        catch(Exception exc) {
            exc.printStackTrace();
        }
        
        return result;
    }
    
    @Override
    public void shutdown() {
        orb.shutdown(false);
    }
    
    @Override
    public String listCategories() {
        String listCategories = "";
        try {
            /*
            Class.forName("com.mysql.jdbc.Driver");
            
            Connection cn = DriverManager.getConnection("jdbc:mysql://localhost:80/bd_corba", "root", "");
            */
            String sentence = "select * from Category";
            
            Statement st = cn.createStatement();
            
            ResultSet rs = st.executeQuery(sentence);
            
            while(rs.next()) {
                listCategories += rs.getString(1) + "-" + rs.getString(2) + "%";
            }
            
            rs.close();
        }
        catch(Exception exc) {
            JOptionPane.showMessageDialog(null, exc.getMessage());
        }
        
        return listCategories;
    }
    
    @Override
    public String returnProduct(String code) {
        String product = "";
        try {/*
            Class.forName("com.mysql.jdbc.Driver");
            
            Connection cn = DriverManager.getConnection("jdbc:mysql://localhost:80/bd_corba", "root", "");
            */
            String sentence = "select * from Product where code = '" + code + "'";
            
            Statement st = cn.createStatement();
            
            ResultSet rs = st.executeQuery(sentence);
            
            while(rs.next()) {
                product += rs.getString(1) + "%" + rs.getString(2)+ "%" + rs.getString(3)+ "%" + rs.getString(4)+ "%" + rs.getString(5);
            }
            
            rs.close();
        }
        catch(Exception exc) {
            JOptionPane.showMessageDialog(null, exc.getMessage());
        }
        
        return product;
    }
    
    @Override
    public boolean updateInformation(String code, String description, String codeCategory, String price, String stock) {
        boolean result =false;
        try {/*
            Class.forName("com.mysql.jdbc.Driver");
            
            Connection cn = DriverManager.getConnection("jdbc:mysql://localhost:80/bd_corba", "root", "");
                */
            String sentence = "update Product set description = '" + description + "', "
                    + "codeCategory = '" + codeCategory + "', "
                    + "priceLastPurchase = '" + price + "', "
                    + "stock = '" + stock + "' where Product.code = '" + code + "'";
           
            Statement st = cn.createStatement();
            
            int value = st.executeUpdate(sentence);
            
            if(value <= 0) {
                result = false;
            }else {
                result = true;
            }
            
            st.close();
        }
        catch(Exception exc) {
            JOptionPane.showMessageDialog(null, exc.getMessage());
        }
        
        return result;
    }
/*
    public static void main(String []args){
        ImplementProduct ewe = new ImplementProduct();
        String ewewe = ewe.listProducts("aaa");
        String a = ewe.listCategories();
        String b = ewe.returnProduct("4");
        ewe.updateInformation("4", "sigo siendo te :v", "4", "1", "12");
        System.out.println(ewe.listProducts("aaa"));
    }
  */      
}
