
package Connection;
import java.sql.*;
/**
 *
 * @author tatsudio
 */
public class ConnectionSQLITE {
    public static Connection get( )
    {
      Connection c = null;
      try {
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:/home/tatsudio/Desktop/CORBA/SistemaProductos/src/ProductBD.sqlite");
        return c;
      } catch ( Exception e ) {
        System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        System.exit(0);
      }
      System.out.println("Opened database successfully");
      return null;
    }
}
