package ProductApp;

/**
* ProductApp/ProductHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from ProductApp.idl
* Friday, April 21, 2017 9:43:36 PM PET
*/

public final class ProductHolder implements org.omg.CORBA.portable.Streamable
{
  public ProductApp.Product value = null;

  public ProductHolder ()
  {
  }

  public ProductHolder (ProductApp.Product initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = ProductApp.ProductHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    ProductApp.ProductHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return ProductApp.ProductHelper.type ();
  }

}
