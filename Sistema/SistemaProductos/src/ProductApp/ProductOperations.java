package ProductApp;


/**
* ProductApp/ProductOperations.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from ProductApp.idl
* Friday, April 21, 2017 9:43:36 PM PET
*/

public interface ProductOperations 
{
  String listProducts (String category);
  String listCategories ();
  boolean eliminateProduct (String code);
  String returnProduct (String code);
  boolean updateInformation (String code, String description, String codeCategory, String price, String stock);
  void shutdown ();
} // interface ProductOperations
